//module dependancies
var mongoose = require('mongoose'),
        http= require('http');
           _ = require('underscore'),
     request = require('request'),
     cheerio = require('cheerio');

process.on('uncaughtException', function (err) {
  console.log('Caught exception: ' + err);
});

//connect to mongodb (currently cloud hosted at mongolab)
mongoose.connect('mongodb://stonecold:GomNom0120@ds043378.mongolab.com:43378/gw2eventstate');

//test db
//mongoose.connect('mongodb://fblfbl:GomNom0120@ds043358.mongolab.com:43358/gw2eventstate');

//create new mongoose database schema for eventNames
var eventNamesSchema = new mongoose.Schema({
  "eventNames" : Object,
  "date" : {
    type: Date, 
    default: Date.now
  }
});

//create a new mongoose database schema for event metaData
var eventMetaSchema = new mongoose.Schema({
  "event_id" : String,
  "event_name": String,
  "location" : String,
  "map" : String,
  "questChain": String,
  "level": String,
  "eventType": String,
  "eventTypeIconAlt": String,
  "eventTypeIconSrc": String,
  "precededBy": String,
  "followedBy": String,
  "date" : {
    type: Date, 
    default: Date.now
  },
  "mapImageUrl": String,
  "objectives": Array
});

var eventMetaMapSchema = new mongoose.Schema({
  "eventMetaMap" : Object,
  "date" : {
    type: Date, 
    default: Date.now
  }
});

var EventMeta = mongoose.model('EventMeta', eventMetaSchema);

var EventMetaMap = mongoose.model('EventMetaMap', eventMetaMapSchema);

var EventNames = mongoose.model('EventNames', eventNamesSchema);

EventNames.findOne().sort({'date': -1}).limit(1).exec(function(err, docs) {

  var events = buildEventNameArray(docs.eventNames);
  scrape(events);
});

function scrape(events) {

  if (!events.length) {
    console.log('*******************\nSCRAPING DONE!!!!!!\n*******************');
    postScrape();
    return;
  }

  var name = events[0].name;
  var id   = events[0].id;

  EventMeta.find({"event_name": name}).limit(1).exec(function(err, docs) {

    if (docs.length) {
      console.log('Event meta already exists!!');
      setTimeout(function() {
        events.shift();
        scrape(events);
      }, 100)
    }
    else {
      console.log('***************************************************\nFetching event meta for: ' + name + '\n***************************************************');
      request(toWikiUrl(name), function(error, response, body) {
        if (!error && response.statusCode == 200) {
          try {
            var $ = cheerio.load(body);

            var dbObj = {};

            dbObj.event_id = id;
            dbObj.event_name = name;

            //the main infobox element
            var infobox = $('.infobox');

            if (!infobox.length) {
              console.log('Infobox doesnt exist..');
              setTimeout(function() {
                events.shift();
                scrape(events);
              }, 5000)
              return;
            }
            //location
            var locationRef = infobox.find('[title="Location"]').parent().next();
            if (locationRef.length) {
              dbObj.location = $(locationRef.find('a')[0]).attr('title');
              dbObj.map = $(locationRef.find('a')[1]).attr('title');
            }

            //event type
            var eventType = infobox.find('dt:contains("Event type")');

            if (eventType.length) {
              dbObj.eventType = infobox.find('dt:contains("Event type")').next().find('a').attr('title');
              dbObj.eventTypeIconAlt = infobox.find('dt:contains("Event type")').next().find('img').attr('alt');
              dbObj.eventTypeIconSrc = infobox.find('dt:contains("Event type")').next().find('img').attr('src');
            }

            //level
            var levelRef = infobox.find('dt:contains("Level")');
            if (levelRef.length) {
              dbObj.level = infobox.find('dt:contains("Level")').next().text().trim();
            }

            //preceeded by
            var precededByRef = infobox.find('dt:contains("Preceded by")');

            if (precededByRef.length) {
              dbObj.precededBy = infobox.find('dt:contains("Preceded by")').next().text().trim();
            }

            //followed by
            //preceeded by
            var followedByRef = infobox.find('dt:contains("Followed by")');

            if (followedByRef.length) {
              dbObj.followedBy = infobox.find('dt:contains("Followed by")').next().text().trim();
            }

            //questChain
            var questChainRef = infobox.find('dt:contains("Part of")');

            if (questChainRef.length) {
              dbObj.questChain = infobox.find('dt:contains("Part of")').next().text().trim();
            }    

            //image
            var imageRef = infobox.find('table .image');

            if (imageRef.length) {
              //get full image of event
              var imgUrlSplit = infobox.find('table .image').find('img').attr('src').replace('/thumb', '').split('/');
              imgUrlSplit.pop();
              dbObj.mapImageUrl = imgUrlSplit.join('/');
            }       

            //objectives
            var objArr = [];

            if ($("#Objectives").length) {
              $("#Objectives").parent().next().children().each(function(k, v) { 
                if ($(v).text().trim())
                  objArr.push($(v).text().trim());
              });
            }
            dbObj.objectives = objArr;

            //save to mongodb
            new EventMeta(dbObj).save(function(err) {
              if (err) {
                console.log('Failed to save!');
              }
              console.log(dbObj);
              setTimeout(function() {
                events.shift();
                scrape(events);
              }, 5000);
            });  
          }
          catch (e) {
            console.log(e);
            setTimeout(function() {
              events.shift();
              scrape(events);
            }, 5000);
          }
        }
        else {
          console.log('Error loading the page, moving on..');
          setTimeout(function() {
            events.shift();
            scrape(events);
          }, 5000);
        }
      });
    }
  });
}

//get eventNames from the database
function postScrape() {
  EventMeta.find().exec(function(err, docs) {
    var newObj = {};

    for (var i = 0; i < docs.length; i++) {
      if (docs[i].event_name)
        newObj[docs[i].event_name.substring(0, docs[i].event_name.length - 1)] = docs[i];
    }

    new EventMetaMap({"eventMetaMap": newObj}).save(function(err) {
      console.log(err)
      process.exit();
    });
  });
}

function buildEventNameArray(eventNames) {
  
  var arr = [];
  var nameCompletedMap = {};
  _.each(eventNames, function(name, id) {

    if (nameCompletedMap[name]) {
      return;
    }

    arr.push({
      "id": id,
      "name": name
    })
    nameCompletedMap[name] = true;
  });
  return arr;
}

function toWikiUrl(eventName) {
  return 'http://wiki.guildwars2.com/wiki/' + eventName.replace(/\./g, '').replace(/ /g, '_');
}
