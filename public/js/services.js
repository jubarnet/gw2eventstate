(function() {
  'use strict';
})();

angular.module('myApp.services', []).value('version', '0.1');
  
/**
 * EventsModel - factory service for calling our server api and updating the angular eventModel
 * @param service $http - angularjs $http service
 * @return obj - service api
 */
angular.module('myApp.services', [])
  .factory('EventsModel', [ '$http', function($http) {

    var eventsModel = {};
    //deep copy the gw2 model to remove the reference to window
    angular.copy(window.gw2.eventsModel, eventsModel);

    var getCurrWorldId = function() {
      return eventsModel.currWorldId;
    };

    var getCurrWorldName = function() {
      return eventsModel.currWorldName;
    };

    var getCurrMapId = function() {
      return eventsModel.currMapId;
    };

    var getCurrMapName = function() {
      return eventsModel.currMapName;
    };

    var getWorldNames = function() {
      return eventsModel.worldNames;
    };

    var getMapNames = function() {
      return eventsModel.mapNames;
    };

    var getEvents = function() {
      return eventsModel.events;
    };

    var getCofEvents = function() {
      return eventsModel.cofevents;
    }

    var getCoeEvents = function() {
      return eventsModel.coeevents;
    }

    /**
     * mapWorldNameToWorldName
     * maps a world name to a world name, returns the world name
     */
    var mapWorldIdToWorldName = function(worldId, worldNames) {
      for (var i = 0; i < worldNames.length; i++) {
        if (worldId === worldNames[i].id) {
          return worldNames[i].name;
        }
      }
      return '';
    };

    /**
     * mapMapNameToMapName 
     * maps a map name to a map names, returns the map name
     */
    var mapMapIdToMapName = function(mapId, mapNames) {
      for (var i = 0; i < mapNames.length; i++) {
        if (mapId === mapNames[i].id) {
          return mapNames[i].name;
        }
      }
      return '';
    };
    
    /**
     * updateWorldEvents
     * uses an id and updates the eventsModel for events, currWorldId, and currWorldName and returns an $http promise
     */
    var updateWorldEvents = function(id) {
      //build the api request url with parameters
      var requestUrl = buildApiUrl(eventsModel.url.api.events, id, eventsModel.currMapId);
      //call api service and return angular $http promise
      return $http.get(requestUrl)
        .success(function(data, status, headers, config) {
          if (data && status === 200) {
            try {
              //success
              angular.copy(data.events, eventsModel.events); //deep copy so the view gets updated
              addEventNamesToEventsObj(eventsModel.eventNames, eventsModel.events);
              eventsModel.currWorldId = id;
              eventsModel.currWorldName = mapWorldIdToWorldName(id, eventsModel.worldNames);
              return true;
            }
            catch (e) {
              return false;
            }
          }
          else {
            return false;
          }
        }).
        error(function(data, status, headers, config) {
          //error
          return false;
        });
    };

    /**
     * updateMapEvents
     * uses an id and updates the eventsModel for events, currMapId, and currMapName and returns angular $http promise
     */
    var updateMapEvents = function() {
      //build the api request url with parameters
      var requestUrl = eventsModel.url.events + '?' + 'world_id=' + eventsModel.currWorldId + '&map_id=' + eventsModel.currMapId
      //call api service and return angular $http promise
      return $http.get(requestUrl)
        .success(function(data, status, headers, config) {
          if (data && status === 200) {
            try {
              //success
              angular.copy(data.events, eventsModel.events); //deep copy so the view gets updated
              addEventNamesToEventsObj(eventsModel.eventNames, eventsModel.events);
              alphabetizeList(eventsModel.events);
            }
            catch (e) {
              return false;
            }
          }
          else {
            return false;
          }
        }).
        error(function(data, status, headers, config) {
          //error
          return false;
        });
    };

    var getEventMeta = function() {

      return $http.get('/api/event_meta')
        .success(function(data, status, headers, config) {
          if (data && status === 200) {
            try {
              return data;
            }
            catch (e) {
              return false;
            }
          }
          else {
            return false;
          }
        }).
        error(function(data, status, headers, config) {
          //error
          return false;
        });
    }

    /**
     * updateCofEvents
     * uses an id and updates the eventsModel for events, and returns angular $http promise
     */
    var updateCofEvents = function(event_id, map_id, eventType) {
      //build the api request url with parameters
      var requestUrl = 'https://api.guildwars2.com/v1/events.json?' + 'event_id=' + event_id + '&map_id=' + map_id;
      //call api service and return angular $http promise
      return $http.get(requestUrl)
        .success(function(data, status, headers, config) {
          if (data && status === 200) {
            try {
              angular.copy(data.events, eventsModel[eventType]); //deep copy so the view gets updated
              addWorldNamesToEventsObj(eventsModel.worldNames, eventsModel[eventType]);
              alphabetizeWorldList(eventsModel[eventType]);
            }
            catch (e) {
              return false;
            }
          }
          else {
            return false;
          }
        }).
        error(function(data, status, headers, config) {
          //error
          return false;
        });
    };

    var getEventTypeImage = function(key) {
    };

    ////////////////////////////////////
    //                                //
    //        Private Functions       //
    //                                //
    ////////////////////////////////////

    /**
     * alphabetizeList - takes a list and returns a sorted list, takes an optional bool argument to reverse sort
     */
    function alphabetizeList(list) {
      return list.sort(function(a, b) {
        //if the optional bool argument is present and its true
        if (arguments.length === 2 && arguments[1]) {
          return a.name < b.name ? -1 : 1
        }
        return a.name > b.name ? -1 : 1
      });
    }

    /**
     * alphabetizeWorldList - takes a list and returns a sorted list, takes an optional bool argument to reverse sort
     */
    function alphabetizeWorldList(list) {
      return list.sort(function(a, b) {
        //if the optional bool argument is present and its true
        if (arguments.length === 2 && arguments[1]) {
          return a.worldName < b.worldName ? -1 : 1
        }
        return a.worldName > b.worldName ? -1 : 1
      });
    }

    /**
     * addEventNamesToEventsObj 
     * adds eventName prop to events by matching the name to an id and return events obj
     */
    function addWorldNamesToEventsObj(worldNames, events) {
      for (var i = 0; i < events.length; i++) {
        for (var j = 0; j < worldNames.length; j++) {
          if (worldNames[j].id == events[i].world_id) {
            events[i].worldName = worldNames[j].name;
          }
        }
      }
      return events;
    }

    /**
     * addEventNamesToEventsObj - adds eventName prop to events by matching the name to an id and return events obj
     */
    function addEventNamesToEventsObj(eventNames, events) {
      for (var i = 0; i < events.length; i++) {
        //if the events id is found in the eventNames map, add a name prop to the event obj
        if (eventNames[events[i].event_id]) {
          events[i].name = eventNames[events[i].event_id];
        }
      }
      return events;
    }

    /**
     * buildApiUrl - takes a url, and an unlimited number of params to create [ buildApiUrl('http://hi.com/', 12, article, 2) ]
     * the new url str with the params string appended to it [ http://hi.com/12/article/2 ]
     */
    function buildApiUrl(url) {
      var paramsList = [];
      for (var i = 1; i < arguments.length; i++) {
        paramsList.push(arguments[i]);
      }
      return url + '/' + paramsList.join('/');
    }

    ////////////////////////////////////
    //                                //
    //              API               //
    //                                //
    ////////////////////////////////////

    return {
      "getCurrWorldId"        : getCurrWorldId,
      "getCurrWorldName"      : getCurrWorldName,
      "getCurrMapId"          : getCurrMapId,
      "getCurrMapName"        : getCurrMapName,
      "getWorldNames"         : getWorldNames,
      "getMapNames"           : getMapNames,
      "getEvents"             : getEvents,
      "mapWorldIdToWorldName" : mapWorldIdToWorldName, 
      "mapMapIdToMapName"     : mapMapIdToMapName,
      "updateWorldEvents"     : updateWorldEvents,
      "updateMapEvents"       : updateMapEvents,
      "getCofEvents"          : getCofEvents,
      "getCoeEvents"          : getCoeEvents,
      "updateCofEvents"       : updateCofEvents,
      "getEventMeta"          : getEventMeta
    };
  }]);