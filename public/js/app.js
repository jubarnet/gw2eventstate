(function() {
  'use strict';  
})();

////////////////////////////////////
//                                //
//              App               //
//                                //
////////////////////////////////////

//Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.services', 'myApp.controllers', 'myApp.directives']).
  config(['$httpProvider', function($httpProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
  }]);

