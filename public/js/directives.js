angular.module('myApp.directives', []).

directive('ngFilter', function() {
  //filters results
  return {
    link: function(scope, element, attr) {
      if (scope.freshEvents) {
        return;
      }
      scope.$watch(attr.ngFilter, function(q) {
        $(element).find('tr:not(.headings)').each(function(i,a) {
          if (q) {
            $(a).toggle(new RegExp(q.toLowerCase()).test($(a).find('.nameLink').text().toLowerCase()));
          }
          else {
            $(a).show();
          }
        });
      });
    }
  };
});