(function() {
  'use strict';  
})();

////////////////////////////////////
//                                //
//          Controllers           //
//                                //
////////////////////////////////////

angular.module('myApp.controllers', []).
  controller('EventsCtrl', ['$scope', '$location', '$window', 'EventsModel',
    function($scope, $location, $window, EventsModel) {

      /****** SCOPE VARS ******/

      //model vars
      $scope.events           = EventsModel.getEvents();
      $scope.currWorldName    = EventsModel.getCurrWorldName();
      $scope.currWorldId      = EventsModel.getCurrWorldId();
      $scope.currMapName      = EventsModel.getCurrMapName();
      $scope.currMapId        = EventsModel.getCurrMapId();
      //timer vars
      $scope.refreshEvents    = true;  //whether checkbox is selected or not
      $scope.freshEvents      = false; //updated events from JS
      $scope.seconds          = 30;    //the event refresh time in seconds
      $scope.isRefreshing     = false; //currently in refresh state

      //local storage wrapper object
      var storage = new Lawnchair({ name : "GW2es", adapter : "dom" }, function(e) {});

      //try to fetch results from local storage, or call our api and store it in local storage
      try {
        storage.exists("meta", function(exists) {
          if (exists) {
            storage.get("meta", function(data) {
              var meta = JSON.parse(data.value); 
              //make scope function available to view
              $scope.getEventMeta = function(name, attr) {
                return getEventMeta(meta, name, attr);
              };
            });
          }
          else {
            EventsModel.getEventMeta().then(function(data) {
              var meta = data.data;
              //make scope function available to view
              $scope.getEventMeta = function(name, attr) {
                return getEventMeta(meta, name, attr);
              };
              //save the meta data to local storage
              storage.save({ key : "meta", value : JSON.stringify(meta) });
            });
          }
        });
      }
      catch (e) {
        EventsModel.getEventMeta().then(function(data) {
          var meta = data.data;
          //make scope function available to view
          $scope.getEventMeta = function(name, attr) {
            return getEventMeta(meta, name, attr);
          };
          //save the meta data to local storage
          storage.save({ key : "meta", value : JSON.stringify(meta) });
        });
      }

      //create a new event timer and start counting down
      new Countdown($scope).start();

      /****** SCOPE FUNCTIONS ******/

      $scope.updateServerEvents = function(worldId) {
        $window.location.href = encodeUrlQueryValue('/events/' + EventsModel.mapWorldIdToWorldName(worldId, EventsModel.getWorldNames()) + '/' + $scope.currMapName, '_self');
      };

      $scope.updateMapEvents = function(mapId) {
        $window.location.href = encodeUrlQueryValue('/events/' + $scope.currWorldName + '/' + EventsModel.mapMapIdToMapName(mapId, EventsModel.getMapNames()), '_self');
      };

      //toggles the event timer
      $scope.toggleRefresh = function(val) {
        $scope.refreshEvents = val;
        if ($scope.refreshEvents) {
          new Countdown($scope).start();
        }
      };

      $scope.getIconSrc = function(src) {
        if (src) {
          var parts = src.split('/');
          return '/img/' + parts[parts.length - 1].replace(/%28/g, '(').replace(/%29/g, ')');
        }
        return '';
      };

      //encodes an event name to a gw2 wiki parameter
      $scope.encodeEventNameToWikiUrl = function(eventName) {
        return 'http://wiki.guildwars2.com/wiki/' + eventName.replace(/\./g, '').replace(/ /g, '_');
      }

      ////////////////////////////////////
      //                                //
      //        Private Functions       //
      //                                //
      ////////////////////////////////////

      function getEventMeta(meta, name, attr) {
        var key = name.substring(0, name.length - 1); //take off the '.' at the end of event name
        if (meta[key] && meta[key][attr]) {
          var val = meta[key][attr];
          if (attr === 'location' || attr === 'level') {
            if (parseInt(val)) {
              return val;
            }
            if (val.toLowerCase().indexOf("varies") !== -1) {
              return 'Varies';
            }
            if (val.toLowerCase().indexOf('various') !== -1) {
              return 'Various';
            }
            if (val.toLowerCase().indexOf('Unspecified') !== -1) {
              return 'Unspecified';
            }
          }
          if (attr === 'eventTypeIconSrc') {
            return val ? 'http://wiki.guildwars2.com' + val : '';
          }
          if (attr === 'mapImageUrl') {
            return val.indexOf('jpg') !== -1 || val.indexOf('png') !== -1 ? val : '';
          }
          return val;
        }
        return '';
      }
      
      function encodeUrlQueryValue(url) {
        return url.replace(/ /g, '-').replace(/'/g, '').toLowerCase();
      }

      //the timer for the event refresh. call using 'new' and pass in scope
      function Countdown(scope) {
        var timer,
        instance = this,

        counterEnd = function() {
          EventsModel.updateMapEvents().then(function() {
            if (!scope.freshEvents) {
              scope.freshEvents = true;
            }
            scope.isRefreshing = false;
            new Countdown(scope).start();
          });
        };

        function decrementCounter() {
          if (!scope.refreshEvents) {
            instance.stop();
          }
          if (scope.seconds === 0) {
            counterEnd();
            instance.stop();
            return;
          }
          scope.seconds--;       
          if (scope.seconds === 0) {
            scope.isRefreshing = true;
          }
          scope.$apply(); //update the scope digest
        }

        this.start = function () {
          clearInterval(timer);
          scope.seconds = 30;
          timer = setInterval(decrementCounter, 1000);
        };

        this.stop = function () {
          clearInterval(timer);
        };
      }

      //tabs
      jQuery('.tab-item').click(function() {
        $('.tab-item').removeClass('active');
        $(this).addClass('active');
      });
    }]).
  controller('CofEventsCtrl', ['$scope', '$location', '$window', 'EventsModel', 
    function($scope, $location, $window, EventsModel) {

      //special ids for COF and COE
      var COF_EVENT = {
        "map_id": "22",
        "event_id": "A1182080-2599-4ACC-918E-A3275610602B"
      };
      var COE_EVENT = {
        "map_id": "39",
        "event_id": "9752677E-FAE7-4F56-A48A-275329095B8A"
      };

      ////////////////////////////////////
      //                                //
      //           Scope vars           //
      //                                //
      ////////////////////////////////////

      $scope.cofevents        = EventsModel.getCofEvents();
      $scope.coeevents        = EventsModel.getCoeEvents();
      $scope.currWorldName    = EventsModel.getCurrWorldName();
      $scope.currWorldId      = EventsModel.getCurrWorldId();
      $scope.currMapName      = EventsModel.getCurrMapName();
      $scope.currMapId        = EventsModel.getCurrMapId();
      $scope.refreshEvents    = true;
      $scope.freshEvents      = false; //updated events from JS
      $scope.seconds          = 30;
      $scope.isRefreshing     = false;

      new Countdown($scope).start();

      ////////////////////////////////////
      //                                //
      //         Scope Functions        //
      //                                //
      ////////////////////////////////////

      /**
       * toggleRefresh
       * toggles refresh on or off
       */
      $scope.toggleRefresh = function(val) {
        $scope.refreshEvents = val;
        if ($scope.refreshEvents) {
          new Countdown($scope).start();
        }
      };

      ////////////////////////////////////
      //                                //
      //        Private Functions       //
      //                                //
      ////////////////////////////////////
      

      /**
       * Countdown
       * takes a scope and sets a timer for event refresh
       */ 
      function Countdown(scope) {
        var timer,
        instance = this,

        counterEnd = function() {
          EventsModel.updateCofEvents(COF_EVENT.event_id, COF_EVENT.map_id, 'cofevents').then(function() {

          });
          EventsModel.updateCofEvents(COE_EVENT.event_id, COE_EVENT.map_id, 'coeevents').then(function() {
            if (!scope.freshEvents) {
              scope.freshEvents = true;
            }
            scope.isRefreshing = false;
            new Countdown(scope).start();
          });
        };

        function decrementCounter() {
          if (!scope.refreshEvents) {
            instance.stop();
          }
          if (scope.seconds === 0) {
            counterEnd();
            instance.stop();
            return;
          }
          scope.seconds--;       
          if (scope.seconds === 0) {
            scope.isRefreshing = true;
          }
          scope.$apply(); //update the scope digest
        }

        this.start = function () {
          clearInterval(timer);
          scope.seconds = 30;
          timer = setInterval(decrementCounter, 1000);
        };

        this.stop = function () {
          clearInterval(timer);
        };
      }
    }]);