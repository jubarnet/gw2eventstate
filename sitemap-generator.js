var 
  mongoose = require('mongoose'),
  request  = require('request'),
  fs       = require('fs');

//get world name
var worldOptions = {
  "url": "http://www.gw2es.com/api/world_names",
  "json": true
};

//start process by getting world names
request(worldOptions, function(error, response, body) {
  if (!error && response.statusCode == 200) {
    mapNames(body); //start mapnames process and pass in worldNames
  }
  else {
    console.log('something went wrong!');
    process.exit();  
  }
});

var mapOptions = {
  "url": "http://www.gw2es.com/api/map_names",
  "json": true
};

function mapNames(worldNames) {
  request(mapOptions, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      generateSitemap(worldNames, body); //generate sitemap using the world names and map names
    }
    else {
      console.log('something went wrong!');
      process.exit();  
    }
  });
}

function generateSitemap(worldNames, mapNames) {

  var sitemap = '';

  var begin = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
  var end = '</urlset>';
  var urlBegin = '<url><loc>';
  var urlEnd   = '</loc></url>';

  //urls
  var home = 'http://www.gw2es.com';
  var cof  = '/cof';
  var coe  = '/coe';

  //construct the events portion
  var events = '';
  for (var i = 0; i < worldNames.length; i++) {
    var worldName = worldNames[i].name.replace(/'/g, '').replace('[', '%5B').replace(']', '%5D').replace(/ /g, '-').toLowerCase();
    for (var j = 0; j < mapNames.length; j++) {
      var mapName = mapNames[j].name.replace(/'/g, '').replace(/ /g, '-').toLowerCase();
      events += urlBegin + home + '/' + worldName + '/' + mapName + urlEnd;
    }
  }

  //build the site map
  sitemap += begin;
  //homepage
  sitemap += urlBegin + home + urlEnd;
  //cof/coe
  sitemap += urlBegin + home + '/coe' + urlEnd;
  sitemap += urlBegin + home + '/cof' + urlEnd;
  //events
  sitemap += events;
  //end
  sitemap += end;

  fs.writeFile('public/sitemap.xml', sitemap, function(err) {
    if (err) {
      console.log('failed to write sitemap.xml');
    }
    else {
      console.log('successfully wrote sitemap.xml to public folder!');
    }
    process.exit();
  });
}