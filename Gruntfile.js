module.exports = function(grunt) {

  grunt.initConfig({
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [
          'public/js/lib/lawnchair.js',
          'public/js/lib/lightbox.js',
          'public/js/lib/underscore/underscore.js',
          'public/js/app.js',
          'public/js/services.js',
          'public/js/controllers.js',
          'public/js/directives.js',
          'public/js/googleanalytics.js'
        ],
        dest: 'public/src/js/app.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! GW2EventState <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        src: '<%= concat.dist.dest %>',
        dest: 'public/src/js/app.min.js'
      }
    },
    less: {
      options: {
        yuicompress: true
      },
      dist: {
        src: ['public/css/app.less', 'public/css/*.less'],
        dest: 'public/src/css/app.min.css'
      }
    },
    watch: {
      css: {
        files: "<%= less.dist.src %>",
        tasks: ["less"]
      },
      js_concat: {
        files: "<%= concat.dist.src %>",
        tasks: ['concat']
      },
      js_uglify: {
        files: '<%= uglify.dist.src %>',
        tasks: ['uglify']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['concat', 'uglify', 'less']);
};