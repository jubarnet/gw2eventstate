////////////////////////////////////
//                                //
//       Module Dependancies      //
//                                //
////////////////////////////////////

var express  = require('express'),
    expose   = require('express-expose'),
    mongoose = require('mongoose'),
    routes   = require('./routes'),
    events   = require('./routes/events'),
    cof      = require('./routes/cof')
    api      = require('./routes/api'),
    http     = require('http'),
    path     = require('path');

var app = express();

//connect to mongodb (currently cloud hosted at mongolab)
//mongoose.connect('mongodb://stonecold:GomNom0120@ds043378.mongolab.com:43378/gw2eventstate');

//alternate database
mongoose.connect('mongodb://fblfbl:GomNom0120@ds043358.mongolab.com:43358/gw2eventstate');

////////////////////////////////////
//                                //
//       App configuration        //
//                                //
////////////////////////////////////

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon('public/favicon.ico'));
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

////////////////////////////////////
//                                //
//           App routes           //
//                                //
////////////////////////////////////

//default homepage to world events
app.get('/', events.setWorldNames, events.setMapNames,
  events.setEventNames, events.setEvents, events.list);
app.get('/events/:worldName/:mapName', events.setWorldNames, events.setMapNames, 
  events.setEventNames, events.setEvents, events.list);

//cof
app.get('/cof', cof.setWorldNames, cof.setCofEvents, cof.setCoeEvents, cof.list);
app.get('/coe', cof.setCoe, cof.setWorldNames, cof.setCofEvents, cof.setCoeEvents, cof.list);

////////////////////////////////////
//                                //
//              API               //
//                                //
////////////////////////////////////

app.get('/api/events/:world_id/:map_id', api.fetchEvents);
app.get('/api/world_names', api.fetchWorldNames);
app.get('/api/map_names', api.fetchMapNames);
app.get('/api/event_names', api.fetchEventNames);
app.get('/api/events', api.fetchEvents);
app.get('/api/cofEvents', api.fetchCofEvents);
app.get('/api/coeEvents', api.fetchCoeEvents);
app.get('/api/event_meta', api.fetchAllEventMeta);

//run the server process
http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
