////////////////////////////////////
//                                //
//       Module Dependancies      //
//                                //
////////////////////////////////////

var request    = require('request'),
    _          = require('underscore'),
    WorldNames = require('./models/worldNames'),
    MapNames   = require('./models/mapNames'),
    EventNames = require('./models/eventNames'),
    Events     = require('./models/events'),
    COFEvent   = require('./models/cofEvent'),
    COEEvent   = require('./models/coeEvent'),
    EventMeta  = require('./models/eventMeta');

////////////////////////////////////
//                                //
//        module vars             //
//                                //
////////////////////////////////////

//urls for gw2 api server
var url = {
  "worldNames" : "https://api.guildwars2.com/v1/world_names.json",
  "mapNames"   : "https://api.guildwars2.com/v1/map_names.json",
  "eventNames" : "https://api.guildwars2.com/v1/event_names.json",
  "events"     : "https://api.guildwars2.com/v1/events.json"
};

//special ids for COF and COE
var COF_EVENT = {
  "map_id": "22",
  "event_id": "A1182080-2599-4ACC-918E-A3275610602B"
};
var COE_EVENT = {
  "map_id": "39",
  "event_id": "9752677E-FAE7-4F56-A48A-275329095B8A"
};

/**
 * OVERRIDE TO USE LOCAL VARS UNTIL THEY FIX THE API!!!!!
 */
var disableRemote = true;

//times in milliseconds
var timesInMilliSeconds = {
  "now"   : function() { return new Date().getTime(); },
  "oneSecond" : 1000,
  "halfMinute": 30000,
  "oneMinute" : 60000,
  "oneHour"   : 3600000,
  "oneDay"    : 86400000,
  "oneWeek"   : 604800000,
  "oneMonth"  : this.oneWeek * 4,
  "oneQuarter": this.oneMonth * 3,
  "oneYear"   : this.oneQuarter * 4
};

////////////////////////////////////
//                                //
//        export functions        //
//                                //
////////////////////////////////////

/**
 * fetchWorldNames
 * calls our db or gw2 events api (if expired) and returns the resulting JSON string to the client
 */
exports.fetchWorldNames = function(req, res) {
  //query mongodb for worldNames
  WorldNames.findOne().sort({'date': -1}).limit(1).exec(function(err, docs) {
    if (!err && docs) {
      //compare the date from the db, and the time now, to determine if we should call the gw2 api and update our db
      //if it hasn't been more than a day since last update, else don't update
      if (!disableRemote && (timesInMilliSeconds.now() - new Date(docs.date).getTime() > timesInMilliSeconds.oneDay)) {
        //make the request to the api and send data back to client
        request(url.worldNames, function(error, response, body) {
          if (!error && response.statusCode == 200) {
            //success
            //update the db with new worldNames and date
            docs.worldNames = body;
            docs.date = timesInMilliSeconds.now();
            docs.save(function(err) {
              if (!err) {
                res.send(body);
              }
              else {
                //error
                res.send(500, {
                  error: 'The gw2 api service is down or something went wrong.' 
                }); 
              }
            }); //end docs.save            
          }
          else {
            //error
            res.send(500, { 
              error: 'The gw2 api service is down or something went wrong.' 
            });   
          }
        }); //end request
      }
      else {
        //time hasn't expired, just use our db results
        res.send(docs.worldNames);
      }
    }
    else {
      //worldNames does not exist in the database, create a new 
      request(url.worldNames, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          //success
          //create a new worldNames row in the database
          new WorldNames({ "worldNames": body }).save();
          res.send(body);
        }
        else {
          //error
          res.send(500, { 
            error: 'The gw2 api service is down or something went wrong.' 
          });   
        }
      }); //end request
    }
  });
};

/**
 * fetchMapNames 
 * calls the gw2 events api and returns the resulting JSON string to the client
 */
exports.fetchMapNames = function(req, res) {
  //query mongodb for mapNames
  MapNames.findOne().sort({'date': -1}).limit(1).exec(function(err, docs) {
    if (!err && docs) {
      //compare the date from the db, and the time now, to determine if we should call the gw2 api and update our db
      //if it hasn't been more than a day since last update, else don't update
      if (!disableRemote && (timesInMilliSeconds.now() - new Date(docs.date).getTime() > timesInMilliSeconds.oneDay)) {
        //make the request to the api and send data back to client
        request(url.mapNames, function(error, response, body) {
          if (!error && response.statusCode == 200) {
            //success
            //update the db with new mapNames and date
            docs.mapNames = body;
            docs.date = timesInMilliSeconds.now();
            docs.save(function(err) {
              if (!err) {
                res.send(body);
              }
              else {
                //error
                res.send(500, {
                  error: 'The gw2 api service is down or something went wrong.' 
                }); 
              }
            }); //end docs.save            
          }
          else {
            //error
            res.send(500, { 
              error: 'The gw2 api service is down or something went wrong.' 
            });   
          }
        }); //end request
      }
      else {
        //time hasn't expired, just use our db results
        res.send(docs.mapNames);
      }
    }
    else {
      //mapNames does not exist in the database, create a new 
      request(url.mapNames, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          //success
          //create a new mapNames row in the database
          new MapNames({ "mapNames": body }).save();
          res.send(body);
        }
        else {
          //error
          res.send(500, { 
            error: 'The gw2 api service is down or something went wrong.' 
          });   
        }
      }); //end request
    }
  });
};

/**
 * fetchEventNames 
 * calls the gw2 events api and returns the resulting JSON string to the client
 */
exports.fetchEventNames = function(req, res) {
  //query mongodb for eventNames
  EventNames.findOne().sort({'date': -1}).limit(1).exec(function(err, docs) {
    if (!err && docs) {
      //compare the date from the db, and the time now, to determine if we should call the gw2 api and update our db
      //if it hasn't been more than a day since last update, else don't update
      if (!disableRemote && (timesInMilliSeconds.now() - new Date(docs.date).getTime() > timesInMilliSeconds.oneDay)) {
        //make the request to the api and send data back to client
        request(url.eventNames, function(error, response, body) {
          if (!error && response.statusCode == 200) {
            //success
            //update the db with new eventNames and date
            docs.eventNames = buildEventNamesMap(JSON.parse(body));
            docs.date = timesInMilliSeconds.now();
            docs.save(function(err) {
              if (!err) {
                res.send(docs.eventNames);
              }
              else {
                //error
                res.send(500, {
                  error: 'The gw2 api service is down or something went wrong.' 
                }); 
              }
            }); //end docs.save            
          }
          else {
            //error
            res.send(500, { 
              error: 'The gw2 api service is down or something went wrong.' 
            });   
          }
        }); //end request
      }
      else {
        //time hasn't expired, just use our db results
        res.send(docs.eventNames);
      }
    }
    else {
      //eventNames does not exist in the database, create a new 
      request(url.eventNames, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          //success
          //create a new eventNames row in the database
          var newEvents = buildEventNamesMap(JSON.parse(body));
          new EventNames({ "eventNames": newEvents }).save();
          res.send(newEvents);
        }
        else {
          //error
          res.send(500, { 
            error: 'The gw2 api service is down or something went wrong.' 
          });   
        }
      }); //end request
    }
  });
};

/*
 * fetchEvents 
 * calls the gw2 events api and returns the resulting JSON string to the client
 */
exports.fetchEvents = function(req, res) {
  //make a params obj add properties to it from the req.params obj
  var params = {};
  if (req.params.world_id) {
    params.world_id = req.params.world_id;
  }
  if (req.params.map_id) {
    params.map_id = req.params.map_id;
  }
  if (req.params.event_id) {
    params.event_id = req.params.event_id;
  }

  //build gw2 request string by using the base url, and params obj
  var requestUrl = buildUrlWithQueryString(url.events, params);
  //if the requestUrl is empty, the client didn't specify any parameters so we return an error
  if (requestUrl === url.events) {
    res.send(500, {
      error: 'No parameters specified. Requires world_id, map_id, and/or event_id' 
    });
  }

  //query mongodb for events
  Events.findOne(params).sort({'date': -1}).limit(1).exec(function(err, docs) {
    if (!err && docs) {
      //compare the date from the db, and the time now, to determine if we should call the gw2 api and update our db
      //if it hasn't been more than a day since last update, else don't update
      if (!disableRemote && (timesInMilliSeconds.now() - new Date(docs.date).getTime() > timesInMilliSeconds.oneMinute)) {
        //make the request to the api and send data back to client
        request(requestUrl, function(error, response, body) {
          if (!error && response.statusCode == 200) {
            //success
            //update the db with new events and date
            docs.world_id = params.world_id;
            docs.map_id = params.map_id;
            docs.events = body;
            docs.date = timesInMilliSeconds.now();
            docs.save(function(err) {
              if (!err) {
                res.send(body);
              }
              else {
                //error
                res.send(500, {
                  error: 'The gw2 api service is down or something went wrong.' 
                }); 
              }
            }); //end docs.save            
          }
          else {
            //error
            res.send(500, { 
              error: 'The gw2 api service is down or something went wrong.' 
            });   
          }
        }); //end request
      }
      else {
        //time hasn't expired, just use our db results
        res.send(docs.events);
      }
    }
    else {
      //events does not exist in the database, create a new 
      request(requestUrl, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          //success
          //create a new events row in the database
          new Events({ 
            "events"   : body,
            "world_id" : params.world_id,
            "map_id"   : params.map_id
          }).save();
          res.send(body);
        }
        else {
          //error
          res.send(500, { 
            error: 'The gw2 api service is down or something went wrong.' 
          });   
        }
      }); //end request
    }
  });
};

/*
 * fetchCofEvents
 * calls the gw2 events api and returns the resulting JSON string to the client
 */
exports.fetchCofEvents = function(req, res) {

  //build gw2 request string by using the base url, and params obj
  var requestUrl = buildUrlWithQueryString(url.events, COF_EVENT);
  //if the requestUrl is empty, the client didn't specify any parameters so we return an error
  if (requestUrl === url.events) {
    res.send(500, {
      error: 'No parameters specified. Requires world_id, map_id, and/or event_id' 
    });
  }

  //query mongodb for events
  COFEvent.findOne(COF_EVENT).sort({'date': -1}).limit(1).exec(function(err, docs) {
    if (!err && docs) {
      //compare the date from the db, and the time now, to determine if we should call the gw2 api and update our db
      //if it hasn't been more than a day since last update, else don't update
      if (!disableRemote && (timesInMilliSeconds.now() - new Date(docs.date).getTime() > timesInMilliSeconds.oneMinute)) {
        //make the request to the api and send data back to client
        request(requestUrl, function(error, response, body) {
          if (!error && response.statusCode == 200) {
            //success
            //update the db with new events and date
            docs.event_id = COF_EVENT.event_id;
            docs.map_id = COF_EVENT.map_id;
            docs.events = body;
            docs.date = timesInMilliSeconds.now();
            docs.save(function(err) {
              if (!err) {
                res.send(body);
              }
              else {
                //error
                res.send(500, {
                  error: 'The gw2 api service is down or something went wrong.' 
                }); 
              }
            }); //end docs.save            
          }
          else {
            //error
            res.send(500, { 
              error: 'The gw2 api service is down or something went wrong.' 
            });   
          }
        }); //end request
      }
      else {
        //time hasn't expired, just use our db results
        res.send(docs.events);
      }
    }
    else {
      //events does not exist in the database, create a new 
      request(requestUrl, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          //success
          //create a new events row in the database
          new COFEvent({ 
            "events"   : body,
            "event_id" : COF_EVENT.event_id,
            "map_id"   : COF_EVENT.map_id
          }).save();
          res.send(body);
        }
        else {
          //error
          res.send(500, { 
            error: 'The gw2 api service is down or something went wrong.' 
          });   
        }
      }); //end request
    }
  });
};

/*
 * fetchCoeEvents
 * calls the gw2 events api and returns the resulting JSON string to the client
 */
exports.fetchCoeEvents = function(req, res) {

  //build gw2 request string by using the base url, and params obj
  var requestUrl = buildUrlWithQueryString(url.events, COE_EVENT);
  //if the requestUrl is empty, the client didn't specify any parameters so we return an error
  if (requestUrl === url.events) {
    res.send(500, {
      error: 'No parameters specified. Requires world_id, map_id, and/or event_id' 
    });
  }

  //query mongodb for events
  COEEvent.findOne(COE_EVENT).sort({'date': -1}).limit(1).exec(function(err, docs) {
    if (!err && docs) {
      //compare the date from the db, and the time now, to determine if we should call the gw2 api and update our db
      //if it hasn't been more than a day since last update, else don't update
      if (!disableRemote && (timesInMilliSeconds.now() - new Date(docs.date).getTime() > timesInMilliSeconds.oneMinute)) {
        //make the request to the api and send data back to client
        request(requestUrl, function(error, response, body) {
          if (!error && response.statusCode == 200) {
            //success
            //update the db with new events and date
            docs.event_id = COE_EVENT.event_id;
            docs.map_id = COE_EVENT.map_id;
            docs.events = body;
            docs.date = timesInMilliSeconds.now();
            docs.save(function(err) {
              if (!err) {
                res.send(body);
              }
              else {
                //error
                res.send(500, {
                  error: 'The gw2 api service is down or something went wrong.' 
                }); 
              }
            }); //end docs.save            
          }
          else {
            //error
            res.send(500, { 
              error: 'The gw2 api service is down or something went wrong.' 
            });   
          }
        }); //end request
      }
      else {
        //time hasn't expired, just use our db results
        res.send(docs.events);
      }
    }
    else {
      //events does not exist in the database, create a new 
      request(requestUrl, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          //success
          //create a new events row in the database
          new COEEvent({ 
            "events"   : body,
            "event_id" : COE_EVENT.event_id,
            "map_id"   : COE_EVENT.map_id
          }).save();
          res.send(body);
        }
        else {
          //error
          res.send(500, { 
            error: 'The gw2 api service is down or something went wrong.' 
          });   
        }
      }); //end request
    }
  });
};

exports.fetchEventMeta = function(req, res) {

  var eventObj = {
    "event_name": req.params.event_name
  }
  EventMeta.findOne(eventObj).sort({'date': -1}).limit(1).exec(function(err, docs) {
    console.log(docs);
    if (!err && docs) {
      res.send(docs);
    }
    else {
      //error
      res.send(200, {empty: true});
    }
  });
};

exports.fetchAllEventMeta = function(req, res) {

  EventMeta.findOne().sort({'date': -1}).limit(1).exec(function(err, docs) {
    if (!err && docs) {
      res.send(docs.eventMetaMap);
    }
    else {
      //error
      res.send(200, {empty: true});
    }
  });
};

////////////////////////////////////
//                                //
//       Private Functions        //
//                                //
////////////////////////////////////

/**
 * requestCallback
 * passed in as callback to a guild wars 2 api request
 */
function requestCallback(res) {
  return function(error, response, body) {
    if (!error && response.statusCode == 200) {
      //success
      res.send(body);
    }
    else {
      //error
      res.send(500, { 
        error: 'The gw2 api service is down or something went wrong.' 
      });   
    }
  };
}

/**
 * buildEventNamesMap
 * takes an array of eventName objects, and builds a map of each eventName object's id to a name, returns new map obj
 */
function buildEventNamesMap(eventNames) {
  var eventNamesMap = {};
  for (var i = 0; i < eventNames.length; i++) {
    //if the eventname isn't already in the map, add a property for it
    if (!eventNamesMap[eventNames[i].id]) {
      eventNamesMap[eventNames[i].id] = eventNames[i].name;
    }
  }
  return eventNamesMap;
}

/**
 * buildUrlWithQueryString
 * takes a json object of url req params and returns a query str
 */
function buildUrlWithQueryString(url, params) {
  //if there are no params, return the url
  if (_.size(params) === 0) {
    return url;
  }
  var paramsList = [];
  for (var k in params) {
    if (params.hasOwnProperty(k)) {
      paramsList.push(k + "=" + params[k]);
    }
  }
  return url + '?' + paramsList.join('&');
}
