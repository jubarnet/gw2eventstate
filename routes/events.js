////////////////////////////////////
//                                //
//       Module Dependancies      //
//                                //
////////////////////////////////////

var request = require('request'),
    _       = require('underscore');

////////////////////////////////////
//                                //
//        module vars             //
//                                //
////////////////////////////////////

//urls for gw2 api server and our server
var url = {
  "worldNames" : "https://api.guildwars2.com/v1/world_names.json",
  "mapNames"   : "https://api.guildwars2.com/v1/map_names.json",
  "eventNames" : "https://api.guildwars2.com/v1/event_names.json",
  "events"     : "https://api.guildwars2.com/v1/events.json",
  "api"        : {
    "world_names" : "/api/world_names",
    "map_names"   : "/api/map_names",
    "event_names" : "/api/event_names",
    "events"      : "/api/events"
  }
};

////////////////////////////////////
//                                //
//        export functions        //
//                                //
////////////////////////////////////

/**
 * list - renders the page 
 * pass eventsModel to jade, exposes eventsModel to window in js
 */
exports.list = function(req, res) {

  var eventsModel = {
    "title"         : "GW2ES",
    "worldNames"    : alphabetizeList(req.worldNames),
    "mapNames"      : alphabetizeList(req.mapNames),
    "currWorldId"   : req.worldId,
    "currMapId"     : req.mapId,
    "currWorldName" : req.worldName,
    "eventNames"    : req.eventNames,
    "currMapName"   : req.mapName,
    "events"        : alphabetizeList(addEventNamesToEventsObj(req.eventNames, req.events.events)),
    "url"           : url,
    "displayServerMapInTitle": req.displayServerMapInTitle,
    "activeTab": "home",
    "isHomePage": req.isHomePage
  };

  //expose events model to client
  res.expose(eventsModel, 'gw2.eventsModel');

  //render page with eventsModel
  res.render('events', eventsModel);
};

/**
 * setWorldNames
 * middleware calls the api and sets the worldNames property on the req object
 */
exports.setWorldNames = function(req, res, next) {
  var requestUrl = getServerBaseUrl(req) + url.api.world_names;
  var propName = 'worldNames'; //the property we want this to be named on the req object
  request(requestUrl, middleWareRequestCallback(req, res, next, propName));
};

/**
 * setMapNames
 * middleware calls the api and sets the mapNames property on the req object
 */
exports.setMapNames = function(req, res, next) {
  var requestUrl = getServerBaseUrl(req) + url.api.map_names;
  var propName = 'mapNames'; //the property we want this to be named on the req object
  request(requestUrl, middleWareRequestCallback(req, res, next, propName));
};

/**
 * setEventNames
 * middleware calls the api and sets the eventNames property on the req object
 */
exports.setEventNames = function(req, res, next) {
  var requestUrl = getServerBaseUrl(req) + url.api.event_names;
  var propName = 'eventNames'; //the property we want this to be named on the req object
  request(requestUrl, middleWareRequestCallback(req, res, next, propName));
};

/**
 * setEvents
 * middleware calls the api and sets the events property on the req object
 */
exports.setEvents = function(req, res, next) {
  var worldObj;
  var mapObj;
  //check if there are params for which world/map events to show
  if (req.params.worldName && req.params.mapName) {
    worldObj  = mapWorldNameToWorldObj(decodeUrlQueryValue(req.params.worldName), req.worldNames);
    mapObj    = mapMapNameToMapObj(decodeUrlQueryValue(req.params.mapName), req.mapNames);
    //check if objs are empty, and set props on req
    if (worldObj) {
      req.worldId   = worldObj.worldId;
      req.worldName = worldObj.worldName;  
    }
    if (mapObj) {
      req.mapId     = mapObj.mapId;
      req.mapName   = mapObj.mapName;  
    }
    req.isHomePage = false;
    req.displayServerMapInTitle = true; //whether we want to show the server or Map in the title of the html page
  }
  else {
    //check if objs are empty, and set props on req
    req.worldId   = '1008';
    req.worldName = 'Jade Quarry';  
    req.mapId     = '15';
    req.mapName   = 'Queensdale';  
    req.isHomePage = true;
    req.displayServerMapInTitle = true; //whether we want to show the server or Map in the title of the html page
  }

  //build gw2 request string by using the base url, and params obj
  var requestUrl = buildApiUrl(getServerBaseUrl(req) + url.api.events, req.worldId, req.mapId);
  var propName = 'events'; //the property we want this to be named on the req object
  request(requestUrl, middleWareRequestCallback(req, res, next, propName));
};

////////////////////////////////////
//                                //
//       Private Functions        //
//                                //
////////////////////////////////////

/**
 * middleWareRequestCallback 
 * passed in as callback to a guild wars 2 api request
 */
function middleWareRequestCallback(req, res, next, propName) {
  return function(error, response, body) {
    if (!error && response.statusCode == 200) {
      //success
      req[propName] = JSON.parse(body); //return json string
      next();
    }
    else {
      //error
      res.send(500, { 
        error: 'The gw2 api service is down or something went wrong.' 
      });   
    }
  };
}

/**
 * buildApiUrl 
 * takes a url, and an unlimited number of params to create [ buildApiUrl('http://hi.com/', 12, article, 2 ]
 * the new url str with the params string appended to it [ http://hi.com/12/article/2 ]
 */
function buildApiUrl(url) {
  var paramsList = [];
  for (var i = 1; i < arguments.length; i++) {
    if (arguments[i]) {
      paramsList.push(arguments[i]);  
    }
  }
  return url + '/' + paramsList.join('/');
}

/**
 * getServerBaseUrl 
 * uses the expressjs request object to build the base url of the server
 */
function getServerBaseUrl(req) {
  return req.protocol + '://' + req.headers.host;
}

/**
 * mapWorldNameToWorldObj 
 * maps a world name to a world obj
 */
function mapWorldNameToWorldObj(worldName, worldNames) {
  for (var i = 0; i < worldNames.length; i++) {
    if (worldName.toLowerCase() === worldNames[i].name.replace(/'/g, '').toLowerCase()) {
      return {
        "worldId"   : worldNames[i].id,
        "worldName" : worldNames[i].name
      };
    }
  }
  return '';
}

/**
 * mapMapNameToMapObj 
 * maps a map name to a map obj
 */
function mapMapNameToMapObj(mapName, mapNames) {
  for (var i = 0; i < mapNames.length; i++) {
    if (mapName.toLowerCase() === mapNames[i].name.replace(/'/g, '').toLowerCase()) {
      return {
        "mapId"   : mapNames[i].id,
        "mapName" : mapNames[i].name
      }
    }
  }
  return '';
}

/**
 * addEventNamesToEventsObj 
 * adds eventName prop to events by matching the name to an id and return events obj
 */
function addEventNamesToEventsObj(eventNames, events) {
  for (var i = 0; i < events.length; i++) {
    //if the events id is found in the eventNames map, add a name prop to the event obj
    if (eventNames[events[i].event_id]) {
      events[i].name = eventNames[events[i].event_id]; //event name
      events[i].wikiUrl = 'http://wiki.guildwars2.com/wiki/' + encodeEventNameToWikiUrl(events[i].name);
    }
  }
  return events;
}

/**
 * decodeUrlQueryValue
 * decodes a str value from the url (ex. seafarer%20s-rest -> seafarers rest)
 */
function decodeUrlQueryValue(path) {
  return decodeURIComponent(path.replace(/-/g, ' '));
}

/**
 * alphabetizeList - takes a list and returns a sorted list, takes an optional bool argument to reverse sort
 */
function alphabetizeList(list) {
  return list.sort(function(a, b) {
    //if the optional bool argument is present and its true
    if (arguments.length === 2 && arguments[1]) {
      return a.name < b.name ? -1 : 1
    }
    return a.name > b.name ? -1 : 1
  });
}

/**
 * encodeEventNameToWikiUrl
 * takes a gw2 wiki param and returns an encoded param for the wiki site
 */
function encodeEventNameToWikiUrl(eventName) {
  return eventName.replace(/\./g, '').replace(/ /g, '_');
}