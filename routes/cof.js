////////////////////////////////////
//                                //
//       Module Dependancies      //
//                                //
////////////////////////////////////

var request = require('request'),
    _       = require('underscore');

////////////////////////////////////
//                                //
//        module vars             //
//                                //
////////////////////////////////////

//urls for gw2 api server and our server
var url = {
  "worldNames" : "https://api.guildwars2.com/v1/world_names.json",
  "mapNames"   : "https://api.guildwars2.com/v1/map_names.json",
  "eventNames" : "https://api.guildwars2.com/v1/event_names.json",
  "events"     : "https://api.guildwars2.com/v1/events.json",
  "api"        : {
    "world_names" : "/api/world_names",
    "map_names"   : "/api/map_names",
    "event_names" : "/api/event_names",
    "events"      : "/api/events",
    "cofEvents"   : "/api/cofEvents",
    "coeEvents"   : "/api/coeEvents"
  }
};

////////////////////////////////////
//                                //
//        export functions        //
//                                //
////////////////////////////////////

/**
 * list - renders the page 
 * pass eventsModel to jade, exposes eventsModel to window in js
 */
exports.list = function(req, res) {

  //set the active tab
  var activeTab = 'cof';
  if (req.isCoeView) {
    activeTab = 'coe';
  }

  var eventsModel = {
    "title"         : "GW2ES",
    "currMapId"     : '22',
    "worldNames"    : req.worldNames,
    "cofevents"     : alphabetizeList(addWorldNamesToEventsObj(req.worldNames, req.cofevents.events)),
    "coeevents"     : alphabetizeList(addWorldNamesToEventsObj(req.worldNames, req.coeevents.events)),
    "activeTab"     : activeTab,
    "isCof"         : true
  };

  //expose events model to client
  res.expose(eventsModel, 'gw2.eventsModel');

  //render page with eventsModel
  res.render('cof', eventsModel);
};

/**
 * setWorldNames
 * middleware calls the api and sets the worldNames property on the req object
 */
exports.setWorldNames = function(req, res, next) {
  var requestUrl = getServerBaseUrl(req) + url.api.world_names;
  var propName = 'worldNames'; //the property we want this to be named on the req object
  request(requestUrl, middleWareRequestCallback(req, res, next, propName));
};

/**
 * setCofEvents
 * middleware calls the api and sets the events property on the req object
 */
exports.setCofEvents = function(req, res, next) {
  //build gw2 request string by using the base url, and params obj
  var requestUrl = buildApiUrl(getServerBaseUrl(req) + url.api.cofEvents);
  var propName = 'cofevents'; //the property we want this to be named on the req object
  request(requestUrl, middleWareRequestCallback(req, res, next, propName));
};

/**
 * setCoeEvents
 * middleware calls the api and sets the events property on the req object
 */
exports.setCoeEvents = function(req, res, next) {
  //build gw2 request string by using the base url, and params obj
  var requestUrl = buildApiUrl(getServerBaseUrl(req) + url.api.coeEvents);
  var propName = 'coeevents'; //the property we want this to be named on the req object
  request(requestUrl, middleWareRequestCallback(req, res, next, propName));
};

/**
 * setCoe
 * middleware for telling our view that this is the coe view
 */
exports.setCoe = function(req, res, next) {
  req.isCoeView = true;
  next();
}

////////////////////////////////////
//                                //
//       Private Functions        //
//                                //
////////////////////////////////////

/**
 * middleWareRequestCallback 
 * passed in as callback to a guild wars 2 api request
 */
function middleWareRequestCallback(req, res, next, propName) {
  return function(error, response, body) {
    if (!error && response.statusCode == 200) {
      //success
      req[propName] = JSON.parse(body); //return json string
      next();
    }
    else {
      //error
      res.send(500, { 
        error: 'The gw2 api service is down or something went wrong.' 
      });   
    }
  };
}

/**
 * buildApiUrl 
 * takes a url, and an unlimited number of params to create [ buildApiUrl('http://hi.com/', 12, article, 2 ]
 * the new url str with the params string appended to it [ http://hi.com/12/article/2 ]
 */
function buildApiUrl(url) {
  var paramsList = [];
  for (var i = 1; i < arguments.length; i++) {
    if (arguments[i]) {
      paramsList.push(arguments[i]);  
    }
  }
  return url + '/' + paramsList.join('/');
}

/**
 * getServerBaseUrl 
 * uses the expressjs request object to build the base url of the server
 */
function getServerBaseUrl(req) {
  return req.protocol + '://' + req.headers.host;
}

/**
 * addEventNamesToEventsObj 
 * adds eventName prop to events by matching the name to an id and return events obj
 */
function addWorldNamesToEventsObj(worldNames, events) {
  for (var i = 0; i < events.length; i++) {
    for (var j = 0; j < worldNames.length; j++) {
      if (worldNames[j].id == events[i].world_id) {
        events[i].worldName = worldNames[j].name;
      }
    }
  }
  return events;
}

/**
 * alphabetizeList - takes a list and returns a sorted list, takes an optional bool argument to reverse sort
 */
function alphabetizeList(list) {
  return list.sort(function(a, b) {
    //if the optional bool argument is present and its true
    if (arguments.length === 2 && arguments[1]) {
      return a.worldName < b.worldName ? -1 : 1
    }
    return a.worldName > b.worldName ? -1 : 1
  });
}