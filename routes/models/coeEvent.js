var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var coeeventSchema = new Schema({
  "events" : String,
  "event_id" : String,
  "map_id" : {
    type: String, 
    default: '-1'
  },
  "date" : {
    type: Date, 
    default: Date.now
  }
});

module.exports = mongoose.model('COEEvent', coeeventSchema);