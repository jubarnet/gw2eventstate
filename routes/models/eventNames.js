var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var eventNamesSchema = new Schema({
  "eventNames" : Object,
  "date" : {
    type: Date, 
    default: Date.now
  }
});

module.exports = mongoose.model('EventNames', eventNamesSchema);