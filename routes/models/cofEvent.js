var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var cofeventSchema = new Schema({
  "events" : String,
  "event_id" : String,
  "map_id" : {
    type: String, 
    default: '-1'
  },
  "date" : {
    type: Date, 
    default: Date.now
  }
});

module.exports = mongoose.model('COFEvent', cofeventSchema);