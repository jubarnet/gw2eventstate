var mongoose = require('mongoose');

//create a new mongoose database schema for event metaData
var eventMetaSchema = new mongoose.Schema({
  "eventMetaMap" : Object,
  "date" : {
    type: Date, 
    default: Date.now
  }});

module.exports = mongoose.model('EventMetaMap', eventMetaSchema);