var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var mapNamesSchema = new Schema({
  "mapNames" : String,
  "date" : {
    type: Date, 
    default: Date.now
  }
});

module.exports = mongoose.model('MapNames', mapNamesSchema);