var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var eventsSchema = new Schema({
  "events" : String,
  "world_id" : String,
  "map_id" : String,
  "date" : {
    type: Date, 
    default: Date.now
  }
});

module.exports = mongoose.model('Events', eventsSchema);